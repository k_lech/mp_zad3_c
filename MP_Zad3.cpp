﻿/*
Zaprojektować i zaimplementować klasy punkt i wielobok. Klasa punkt ma odwzorowywać punkt
na płaszczyźnie. Klasa powinna posiadać zbiór konstruktorów umozliwiających właściwe inicjowanie pól
składowych tworzonych obiektów. Winna ona być takŜe wyposazona w metody dostępowe umozliwiające
pobieranie i modyfikowanie wartości pól składowych. Klasa punkt powinna posiadać tez metodę obliczającą
odległość danego punktu od innego.
Klasa wielobok powinna odwzorowywać wieloboczną figurę geometryczną na płaszczyźnie opisaną
zbiorem punktów. Klasa powinna dostarczać konstruktor domyślny oraz konstruktor umozliwiający utworzenie
zbioru punktów opisujących tę figurę. Ponadto klasa powinna dostarczać metody dostępowe dające mozzliwość
odczytu ilości punktów oraz odczytu i zapisu współrzędnych poszczególnych punktów. Klasa wielobok
powinna takze posiadać metodę zmiany zbioru punktów na nowy oraz metodę obliczania obwodu tej figury.
*/
#include "pch.h"
#include <iostream>
#include <cstdlib>
using namespace std;

class punkt
{
	double _x, _y;
public:
	punkt () : _x(0), _y(0) {}						
	punkt(double x, double y) : _x(x), _y(y) {}		
	
	double &x() { return _x; }				
	double &y() { return _y; }

	punkt(const punkt & copy)					
	{
		_x = copy._x;
		_y = copy._y;
	}
	
	double odleglosc(const punkt &pkt) const		
	{
		return sqrt((_x - pkt._x)*(_x - pkt._x) + (_y - pkt._y)*(_y - pkt._y));
	}
};

class wielobok
{
	size_t rozmiar;
	punkt * wsk;
public:
	wielobok () : rozmiar(0), wsk(0) {}				
	wielobok(const punkt *poczatek, const punkt *koniec) : rozmiar(koniec - poczatek), wsk(rozmiar ? new punkt [rozmiar]: 0) 
	{
		for (size_t i = 0; i < rozmiar; i++)
		{
			wsk[i] = poczatek[i];
		}
	}
	~wielobok() { delete[] wsk; }
	
	size_t ilosc()
	{
		return rozmiar;
	}

	double obwod() {
		double sumaObw = 0;
		for (size_t i = 0; i < rozmiar; i++)
		{
			sumaObw += wsk[i].odleglosc(wsk[i + 1]);
			cout << "pkt: " << i << "Wspolrzedne (x, y): " << wsk[i].x() << "  " << wsk[i].y() << endl;
		}
		return sumaObw;
	}

	punkt & Punkt(int n)
	{
		return * (wsk+n);
	}
	void Punkty(const punkt* poczatek, const punkt* koniec)
	{
		rozmiar = koniec - poczatek;
		if (rozmiar > 0)
		{
			for (size_t i = 0; i < rozmiar; i++)
			{
				wsk = new punkt[rozmiar];
			}
		}
		else 
		{ 
			wsk = 0;
			return; 
		}
		for (size_t i = 0; i< rozmiar; i++)
		{
			wsk[i] = poczatek[i];

		}
	}
};

int main()
{
   	punkt p(2, 3);
	cout << p.x() << ' ' << p.y() << '\n';
	p.x() = 1;
	p.y() = 1;
	cout << p.x() << ' ' << p.y() << '\n';
		
	cout << p.odleglosc(punkt()) << '\n';
	punkt t[] = { punkt(0, 1), punkt(0, 0), punkt(1, 0), punkt(1, 1) };		
	
	wielobok w1(t, t + 4);
	cout << w1.obwod() << '\n';
	w1.Punkt(1) = punkt(0.5, 0.5);
	cout << w1.obwod() << '\n';
	wielobok w2;
	w2.Punkty(t, t + 3);
	cout << w2.obwod() << '\n';
	for (size_t i = 0; i < w2.ilosc(); ++i)
		cout << w2.Punkt(i).x() << ' ' << w2.Punkt(i).y() << '\n';
	
	system("Pause");
	return 0;
}
